# Simple Calculator using Shell
#!bin/sh
clear
sum=0
i="y"
echo "Enter one no: "
read n1
echo "Enter second no: "
read n2
while [ $i = "y" ]
do
echo "1. Addition"
echo "2. Subtraction"
echo "3. Multiplication"
echo "4. Division"
echo "5. modulus"
echo "6. exponent"
echo "Enter your choice: "
read ch
case $ch in
        1)sum=$(echo $n1 \+ $n2 | bc)
        echo "Sum ="$sum;;
        2)sum=$(echo $n1 \- $n2 | bc)
        echo "Sub = "$sum;;
        3)sum=$(echo $n1 \* $n2 | bc)
        echo "Mul = "$sum;;
        4)sum=$(echo $n1 \/ $n2 | bc)
        echo "Div = "$sum;;
        5)sum=`expr $n1 % $n2`
        echo "mod = "$sum;;
        6)sum=$(echo $n1 \^ $n2 | bc)
        echo "exp = "$sum;;
        *)echo "Invalid choice!";;
esac
echo "Do u want to continue (y/n) ?"
read i
if [ "$i" -nq "y" ]
then
exit
fi
done
