echo "Operating system Details:"
cat /etc/issue.net
cat /etc/os-release
echo "\nKernel Version:"
uname -r
printf "\nAvailable Shells:"
cat /etc/shells
printf "\nMouse Settings:"
xinput | grep "pointer"
printf "\nComputer CPU Infromation:"
cat /proc/cpuinfo
printf "\nMemory Information:"
cat /proc/meminfo
printf "\nFile system mounted:"
cat /proc/mounts
printf "\nHarddisk Information:"
sudo fdisk -l
